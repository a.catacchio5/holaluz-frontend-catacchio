import axios from 'axios'

export const getClients = async function (){
  const response = await axios.get('/clients')
  return response
}

export const getSupplyPoints = async function (){
  const response = await axios.get('/supply-points')
  return response
}