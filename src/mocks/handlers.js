import { rest } from 'msw'

import * as clients from '../assets/mocks/clients.json';
import * as supplyPoints from '../assets/mocks/supply-points.json';

export default [
    rest.get('/clients', (req, res, ctx) => {
        return res(
            ctx.delay(500),
            ctx.json(clients)
        )
    }),
    rest.get('/supply-points', (req, res, ctx) => {
        return res(
            ctx.delay(500),
            ctx.json(supplyPoints)
        )
    })
]